package com.company.ENTITY;

import java.util.Date;

public class Eleve {

    public int idEleve ;
    public String nom ;
    public String prenom ;
    public Date date_naiss ;
    public String lieu ;
    public String adresse ;
    public int telephone ;

    public Eleve(){

    }

    public Eleve(int idEleve, String nom, String prenom, Date date_naiss, String lieu, String adresse, int telephone) {
        this.idEleve = idEleve;
        this.nom = nom;
        this.prenom = prenom;
        this.date_naiss = date_naiss;
        this.lieu = lieu;
        this.adresse = adresse;
        this.telephone = telephone;
    }

    public int getIdEleve() {
        return idEleve;
    }

    public void setIdEleve(int idEleve) {
        this.idEleve = idEleve;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDate_naiss() {
        return date_naiss;
    }

    public void setDate_naiss(Date date_naiss) {
        this.date_naiss = date_naiss;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

}





