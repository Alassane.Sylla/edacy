package com.company.ENTITY;

import java.util.Date;

public class Classe {

    public int idClasse ;
    public String libelleClasse ;
    // public int numInscription ;
    public int idEleve ;

    public Classe(){

    }

    public Classe(int idClasse, String libelleClasse, int idEleve) {
        this.idClasse = idClasse;
        this.libelleClasse = libelleClasse;
        // this.numInscription = numInscription;
        this.idEleve = idEleve;
    }

    public int getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(int idClasse) {
        this.idClasse = idClasse;
    }

    public String getLibelleClasse() {
        return libelleClasse;
    }

    public void setLibelleClasse(String libelleClasse) {
        this.libelleClasse = libelleClasse;
    }

   /* public int getNumInscription() {
        return numInscription;
    }*/

  /*  public void setNumInscription(int numInscription) {
        this.numInscription = numInscription;
    }*/

    public int getIdEleve() {
        return idEleve;
    }

    public void setIdEleve(int idEleve) {
        this.idEleve = idEleve;
    }


}




