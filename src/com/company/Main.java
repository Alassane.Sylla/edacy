package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.company.DAO.IclasseImp;
import com.company.DAO.IeleveImp;
import com.company.ENTITY.Classe;
import com.company.ENTITY.Eleve;

public class Main {
    public static void main(String[] args) {

        IeleveImp el = new IeleveImp();
        IclasseImp cl = new IclasseImp();
        // IinscriptionImp ins = new IinscriptionImp();


        System.out.println(" FAITES VOTRES CHOIX : \n\t");
        System.out.println(" [1]-AJOUTER UN ELEVE :");
        System.out.println(" [2]-AFFICHER LA LISTE DES ELEVES :"); //LES INSCRITS
        System.out.println(" [3]-AJOUTER UN ELEVE DANS UNE CLASSE :");
        System.out.println(" [4]-AJOUTER UNE NOUVELLE CLASSE :");
        System.out.println(" [5]-AFFICHER LA LISTE DES CLASSES :");
        System.out.println(" [6]-MODIFIER UNE CLASSES :");
        System.out.println(" [7]-SUPPRIMER UNE CLASSE :");

        List<Eleve> listAllElev = null;
        listAllElev = new ArrayList<>();

        List<Classe> listAllCla = null;
        listAllCla= new ArrayList<>();

        int choix = 0 ;
        do {
            // System.out.println(" FAITES VOTRE CHOIX : \n");
            Scanner sc = new Scanner(System.in);
            choix = Integer.parseInt(sc.nextLine()) ;

            //System.out.println("Entrez votre choixs: \n");

            switch (choix) {

                /*
                ajouter éléve....................................
                 */
                case 1:
                    System.out.println(" VOUS AVEZ CHOISI D'AJOUTER UN ELEVE : \n");
                    Eleve nouveauElev = new Eleve();
                    nouveauElev= el.ajoutEleve();
                    listAllElev.add(nouveauElev);
                    refaireChoix();
                    break;

                /*
                afficher liste éléves.............................
                 */
                case 2:
                    System.out.println(" VOUS AVEZ CHOISI D'AFFICHER LA LISTE DES ELEVES : \n");
                    for (int i = 0; i < listAllElev.size(); i++)
                    {
                        if (listAllElev.get(0).getPrenom() == "")
                        {
                            System.out.println("la liste ne contient pas d'éléves!!!!");
                        }
                        else
                            System.out.println("\tELEVE N°" + listAllElev.get(i).getIdEleve() + " - PRENOM : " + listAllElev.get(i).getPrenom() + " - PRENOM : " + listAllElev.get(i).getNom() + " - DATE NAISSANCE : " + listAllElev.get(i).getDate_naiss() + " - LIEU NAISSANCE : " + listAllElev.get(i).getLieu() + " - ADRESSE : " + listAllElev.get(i).getAdresse() + " - TELEPHONE : " + listAllElev.get(i).getTelephone());
                    }
                    refaireChoix();
                    break;

                    /*
                    ajouter un eleve dans un classe..................................
                     */
                case 3:
                    System.out.println(" VOUS AVEZ CHOISI D'AJOUTER UN ELEVE DANS UNE CLASSE :\n");
                    // el.ajouterDansUneClasse();
                    System.out.println("...............");
                    refaireChoix();
                    break;

                    /*
                    ajouter une nouvelle classe.......................................
                     */
                case 4:
                    System.out.println("VOUS AVEZ CHOISI D'AJOUTER UNE NOUVELLE CLASSE : \n");
                    Classe nouveauCla = new Classe();
                    nouveauCla = cl.ajouter();
                    listAllCla.add(nouveauCla);
                    refaireChoix();
                    break;

                     /*
                    afficher la liste des classes........................................
                     */
                case 5:
                    System.out.println(" VOUS AVEZ CHOISI D'AFFICHER LA LISTE DES CLASSES : \n");
                    if (listAllCla == null)
                        System.out.println("la liste est vide !!!");
                    else
                        // System.out.println("non vide");
                        for (int i = 0; i < listAllCla.size(); i++)
                        {
                            System.out.println("\tCLASSE N°" + listAllCla.get(i).getIdClasse() + " - LIBELLE : " + listAllCla.get(i).getLibelleClasse());
                        }
                    refaireChoix();
                    break;

                case 6:
                    System.out.println(" VOUS AVEZ CHOISI DE MODIFIER UNE CLASSE : \n");
                    refaireChoix();

                    /*
                     supprimer une classe
                    */
                case 7:
                    System.out.println("VOUS AVEZ CHOISI DE SUPPRIMER UNE CLASS : \n");
                    System.out.println("ENTREZ L'IDENTIFIANT : \n\t");
                    int saisie = Integer.parseInt(sc.nextLine());
                    if (listAllCla == null)
                        System.out.println("la liste est vide !!!");
                    else
                        System.out.println(" ENTRER L'ID DE LA CLASSE A SUPPRIMER : ");
                    //suppression
                    for (int i = 0; i < listAllCla.size(); i++)
                    {
                        if (listAllCla.get(i).getIdClasse() == saisie){
                            listAllCla.remove(listAllCla.get(i).getIdClasse());
                        }

                    }
                    //affichage
                    for (int i = 0; i < listAllCla.size(); i++)
                    {
                        System.out.println("\tCLASSE N°" + listAllCla.get(i).getIdClasse() + " - LIBELLE : " + listAllCla.get(i).getLibelleClasse());
                    }
                    refaireChoix();

                    break;

                    /*
                     choix inexistant
                    */
                default:
                    if (choix >= 7)
                        System.out.println("CE CHOIX N'EXISTE PAS !!!");
                    refaireChoix();
            }
        } while(choix != 0);



    }
    public static void refaireChoix(){
        System.out.println(" REFAIRE UN CHOIX ");
    }
}
