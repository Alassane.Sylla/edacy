package com.company.DAO;

import com.company.ENTITY.Classe;

import java.util.List;

public interface Iclasse {


    public Classe ajouter();
    public Classe modifier(int idCl, String libcl);
    public List<Classe> supprimer(int idCl);
    public List<Classe> AffcherClasse();
}

