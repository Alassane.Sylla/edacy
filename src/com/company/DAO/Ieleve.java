package com.company.DAO;

import com.company.ENTITY.Eleve;

import java.util.List;

public interface Ieleve {

    public Eleve ajoutEleve();
    public List<Eleve> afficherEleve(Eleve el);

}
